//
//  ViewController.swift
//  SwiftScoreKeeper
//
//  Created by moha on 23/1/2018 AP.
//  Copyright © 1396 moha. All rights reserved.
//

import UIKit

class ViewController: UIViewController , UITextFieldDelegate {
    var team1ScoreLbl = UILabel()
    var team1Stepper = UIStepper()
    
    var team2ScoreLbl = UILabel()
    var team2Stepper = UIStepper()
    
    override func viewDidLoad() {
        
        
        
        super.viewDidLoad()
        
        
        
        let holdingView = UIView()
        holdingView.translatesAutoresizingMaskIntoConstraints = false
        holdingView.backgroundColor = .magenta
        self.view.addSubview(holdingView)
        
        let rightHoldingView = UIView()
        rightHoldingView.translatesAutoresizingMaskIntoConstraints = false
        rightHoldingView.backgroundColor = .blue
        self.view.addSubview(rightHoldingView)
        
        
        
        let team1TxtFld = UITextField()
        team1TxtFld.translatesAutoresizingMaskIntoConstraints = false
        team1TxtFld.backgroundColor = .yellow
        team1TxtFld.text = "China"
        
        //team1TxtFld.becomeFirstResponder()
        
        //UIApplication.shared.sendAction(#selector(UIResponder.resignFirstResponder), to:nil, from:nil, for:nil)

        
        //
        holdingView.addSubview(team1TxtFld)
        
        let team2TxtFld = UITextField()
        team2TxtFld.translatesAutoresizingMaskIntoConstraints = false
        team2TxtFld.backgroundColor = .red
        team2TxtFld.text = "USA"
        rightHoldingView.addSubview(team2TxtFld)
        
        
        
        
        
        team1ScoreLbl = UILabel()
        team1ScoreLbl.translatesAutoresizingMaskIntoConstraints = false
        team1ScoreLbl.backgroundColor = .green
        team1ScoreLbl.text = "0"
         holdingView.addSubview(team1ScoreLbl)
        
        
        team2ScoreLbl = UILabel()
        team2ScoreLbl.translatesAutoresizingMaskIntoConstraints = false
        team2ScoreLbl.backgroundColor = .green
        team2ScoreLbl.text = "0"
        rightHoldingView.addSubview(team2ScoreLbl)
        
        
        
        
        team1Stepper = UIStepper()
        team1Stepper.translatesAutoresizingMaskIntoConstraints = false
        team1Stepper.addTarget(self, action: #selector(stepperValueChanged(_:)), for: .valueChanged)
        team1Stepper.maximumValue = 21
        holdingView.addSubview(team1Stepper)
        
        team2Stepper = UIStepper()
        team2Stepper.translatesAutoresizingMaskIntoConstraints = false
        team2Stepper.addTarget(self, action: #selector(stepperValueChanged(_:)), for: .valueChanged)
        team2Stepper.maximumValue = 21
        rightHoldingView.addSubview(team2Stepper)
        
        
        
        
        let resetBtn = UIButton(type: .system)
        resetBtn.translatesAutoresizingMaskIntoConstraints = false
        resetBtn.backgroundColor = .purple
        resetBtn.setTitle("Reset", for: .normal)
        self.view.addSubview(resetBtn)
        resetBtn.addTarget(self, action: #selector(resetBtnTouched(_:)), for: .touchUpInside)
        
        
        let views: [String: Any] = [
            "team1TxtFld": team1TxtFld,
            "team1ScoreLbl": team1ScoreLbl,
            "team1Stepper": team1Stepper,
            "team2TxtFld": team2TxtFld,
            "team2ScoreLbl": team2ScoreLbl,
            "team2Stepper": team2Stepper,
            "resetBtn": resetBtn,
            "holdingView":holdingView,
            "rightHoldingView":rightHoldingView
        ]
        
        
        var allConstraints: [NSLayoutConstraint] = []
        //var allConstraintsRight: [NSLayoutConstraint] = []
        
        
        
        
        let holdingViewHorizontalConstraints = NSLayoutConstraint.constraints(
            withVisualFormat: "H:|[holdingView]-[rightHoldingView]|",
            metrics: nil,
            views: views)
        allConstraints += holdingViewHorizontalConstraints
        
        let holdingViewVerticalConstraints = NSLayoutConstraint.constraints(
            withVisualFormat: "V:|[holdingView]|",
            metrics: nil,
            views: views)
        allConstraints += holdingViewVerticalConstraints
        
        
        let team1TxtFldConstraints = NSLayoutConstraint.constraints(
            withVisualFormat: "H:|-10-[team1TxtFld]-10-|",
            metrics: nil,
            views: views)
        allConstraints += team1TxtFldConstraints
        
        let team1ScoreLblConstraints = NSLayoutConstraint.constraints(
            withVisualFormat: "H:|-10-[team1ScoreLbl]-10-|",
            metrics: nil,
            views: views)
        allConstraints += team1ScoreLblConstraints
        
        
        let team1StepperConstraints = NSLayoutConstraint.constraints(
            withVisualFormat: "H:|-60-[team1Stepper]",
            metrics: nil,
            views: views)
        allConstraints += team1StepperConstraints
        
        
        
        

        
        let rightHoldingViewHorizontalConstraints = NSLayoutConstraint.constraints(
            withVisualFormat: "H:|-[holdingView]-0-[rightHoldingView]-0-|",
            metrics: nil,
            views: views)
        
        allConstraints += rightHoldingViewHorizontalConstraints
        
        
        
        let rightHoldingViewVerticalConstraints = NSLayoutConstraint.constraints(
            withVisualFormat: "V:|[rightHoldingView]|",
            metrics: nil,
            views: views)
        allConstraints += rightHoldingViewVerticalConstraints
        
        
        
        let team2TxtFldConstraints = NSLayoutConstraint.constraints(
            withVisualFormat: "H:|-10-[team2TxtFld]-10-|",
            metrics: nil,
            views: views)
        allConstraints += team2TxtFldConstraints
        
        
        let team2ScoreLblConstraints = NSLayoutConstraint.constraints(
            withVisualFormat: "H:|-10-[team2ScoreLbl]-10-|",
            metrics: nil,
            views: views)
        allConstraints += team2ScoreLblConstraints
        
        
        let team2StepperConstraints = NSLayoutConstraint.constraints(
            withVisualFormat: "H:|-[holdingView(210)]-60-[team2Stepper]",
            metrics: nil,
            views: views)
        allConstraints += team2StepperConstraints
        
        
        
        
        
        let resetBtnConstraints = NSLayoutConstraint.constraints(
            withVisualFormat: "H:|[resetBtn]|",
            metrics: nil,
            views: views)
        allConstraints += resetBtnConstraints
        
        
        
        
        
        
        
        let verticalConstraints = NSLayoutConstraint.constraints(
            withVisualFormat: "V:|-60-[team1TxtFld]-[team1ScoreLbl(>=120)]-[team1Stepper]-[resetBtn(55)]|",
            metrics: nil,
            views: views)
        allConstraints += verticalConstraints
        
        let rightVerticalConstraints = NSLayoutConstraint.constraints(
            withVisualFormat: "V:|-60-[team2TxtFld]-[team2ScoreLbl(>=120)]-[team2Stepper]-[resetBtn(55)]|",
            metrics: nil,
            views: views)
        allConstraints += rightVerticalConstraints
        
        
        
        NSLayoutConstraint.activate(allConstraints)
        
        

        
    }
    
    @objc func stepperValueChanged(_ sender : UIStepper){
        team1ScoreLbl.text =  String(format:"%.f", team1Stepper.value)
        team2ScoreLbl.text =  String(format:"%.f", team2Stepper.value)
    }
    
    
    
    
    @objc func resetBtnTouched(_ sender : UIButton){
        team1ScoreLbl.text = "0"
        team1Stepper.value = 0;
        
        team2ScoreLbl.text = "0"
        team2Stepper.value = 0;
    }
    

//    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
//        self.view.endEditing(true)
//        return true;
//    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    
   
    
}






